# jm-confetti

a simple class that filters a list of dom elements by a specified data attribute.

## usage

    let confettiInstance = new Confetti({
      parent: '.confetti',
      count: 450,
      particleSize: 10,
      colors: [
        '#ffe77c',
        '#ff7c7c',
        '#ff3030',
        '#ffffff'
      ]
    })

### parent
selector of container to pour confetti into

### count
number of confetti particles

### flakeSize 
size of confetti particles

### colors
an array of any length with css-friendly color definitions

### Confetti.playToggle(true)
gracefully stops/starts confetti shower. adding 'true' argument will force stop.

## demo
[https://jamesmaclennan.bitbucket.io/jm-confetti/](https://jamesmaclennan.bitbucket.io/jm-confetti/)