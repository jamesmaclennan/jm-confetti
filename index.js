import { TweenMax, TimelineLite } from 'gsap';

function range(min, max, floor = true) {
  if (!floor) {
    return (Math.random() * (max - min) + min)
  }
  return Math.floor(Math.random() * (max - min) + min);
}

class Confettiparticle {
  constructor(name, size) {
    this.name = 'particle-' + name;
    this.size = size;
    this.el = null;
    this.playStatus= true;
    
    this.tl = new TimelineLite({paused: true, onComplete: () => {
      if (this.playStatus == true) {
        this.tl.restart();
      }
    }});
  }

  init(config) {
    this.config = {
      flipSpeed: config.flipSpeed,
      verticalTravel: config.verticalTravel,
      verticalSpeed: config.verticalSpeed,
      horizontalTravel: config.horizontalTravel,
      horizontalSpeed: config.horizontalSpeed,
      startDelay: config.startDelay
    }

    TweenMax.to(this.el, this.config.horizontalSpeed, {x: this.config.horizontalTravel, repeat: -1, yoyo: true, ease:Sine.easeInOut})
    TweenMax.to(this.el, this.config.flipSpeed, {rotationY:'+=180', repeat: -1})
    TweenMax.to(this.el, range(.05, .75, false), {rotationX:'+=180', repeat: -1, ease:Linear.easeNone})

    this.tl.addLabel('start');
    this.tl.to(this.el, 2, {opacity: 1});
    this.tl.to(this.el, this.config.verticalSpeed, {y: this.config.verticalTravel, ease:Linear.easeNone}, 'start')
    this.tl.to(this.el, 1, {opacity: 0}, '-=1')
    this.tl.seek('start');

    this.play(this.config.startDelay);
  }

  play() {
    setTimeout(() => {this.tl.play()}, this.config.startDelay);
  }

  stop() {
    this.playStatus = false;
  }

  start() {
    this.play();
    this.playStatus = true;
  }
}

class Confetti {
  constructor(config) {
    this.count = config.count;
    this.particleSize = config.particleSize;
    this.container = document.querySelector(config.parent);
    this.containerWidth = window.outerWidth;
    this.containerHeight = window.outerHeight;
    this.colors = config.colors;
    this.particles = [];
    this.playStatus = true;
    this.playToggleButton = document.createElement('button');
    
    this.createConfettiparticles(this.count);

        // create stop and start buttons
    this.playToggleButton.innerText = 'stop';
    this.playToggleButton.addEventListener('click', () => {
      this.playToggle(this.playStatus);
    })
    this.container.appendChild(this.playToggleButton);
  }

  createConfettiparticles(n) {
    for (var i=0; i <= n; ++i) {
      this.particles[i] = new Confettiparticle(i, this.particleSize);
    }

    this.particles.forEach((particle) => {
      let x = document.createElement('div');
      x.classList.add('particle', particle.name);
      x.style.width = particle.size + 'px';
      x.style.height = particle.size + 'px';
      x.style.left = range(1,100) + '%'
      x.style.top = range(-25, 100) + '%'
      x.style.opacity = 0;
      x.style.position = 'absolute';
      x.style.backgroundColor = this.colors[range(0,(this.colors.length))];

      this.container.appendChild(x);
      particle.el = document.querySelector('.' + particle.name);
      
      particle.init(
        {
          flipSpeed: range(1,5),
          horizontalTravel: range(50, 150),
          horizontalSpeed: range(4, 8, false),
          verticalTravel: range(50, 500),
          verticalSpeed: range(2, 4, false),
          startDelay: range(0, 2000, false)
        }
      )
    })
  }

  playToggle(forceStop = false) {
    if (forceStop || this.playStatus) {
      this.playStatus = false;
      this.particles.forEach((particle) => {
        particle.stop();
        // this.playToggleButton.innerText = 'play'
      })
      return
    }

    this.playStatus = !this.playStatus;
    if (this.playStatus) {
      this.particles.forEach((particle) => {
        particle.start();
        // this.playToggleButton.innerText = 'stop'
      })
    }
  }
}

export default Confetti
